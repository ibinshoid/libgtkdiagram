# libgtkdiagram

GtkDiagram is a widget for Gtk3 which is written in vala.

It can draw bar charts and line charts.

For easy GUI creation it is integrated into glade.

Vala example diagram.vala:

    using Gtk;
    using diagram;
    
    int main (string[] args) {
		Gtk.init (ref args);

	    var window = new Window ();
	    window.title = "First Diagram";
	    window.set_default_size (350, 70);
	    window.destroy.connect (Gtk.main_quit);
	
	    var diagram = new Diagram();
		diagram.werte = {20,80,10,50};
		diagram.type = 1;
	    window.add (diagram);
	    window.show_all ();
	
	    Gtk.main ();
	    return 0;
		
    }

Compile and Run
    
    valac --pkg gtk+-3.0 --pkg gtkdiagram diagram.vala
    ./diagram.vala

Packages for some distributions can be downloaded at http://software.opensuse.org/package/libgtkdiagram .
    
   
