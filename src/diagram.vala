using Gtk;

namespace diagram{
	
	public class Diagram : Widget {
		public double[] werte;
		public double max;
		public int type;
		
		construct{
			set_has_window (false);
			this.werte = {0};
			this.type = 0;
			this.max = 0;
		}
	    
	    public override bool draw (Cairo.Context cr) {
	        int width = get_allocated_width ();
	        int height = get_allocated_height ();
			int i = 0;
			double lastx = 20 + width / werte.length / 3;
			double lasty = 0;

			if (max == 0){
				foreach (double d in werte){
					if (d > max){
						max = d;
					}
				}
			}
	
	        cr.set_source_rgba (0.5, 0.5, 0.5, 1);
			cr.move_to(20, 20);
			cr.line_to(20, height - 20);
			cr.line_to(width, height - 20);
	        cr.stroke ();
			cr.move_to(0, height - 15);
			cr.show_text("0");
			cr.move_to(0, 25);
			cr.show_text(((int)max).to_string());
			foreach (double d in werte){
				if (d <= 0){
					d = 0;
				}
				i += 1;
				cr.move_to(lastx, height - 10);
				cr.show_text((i).to_string());
				if (type == 0){
					cr.rectangle(lastx - (width / werte.length / 4), (height - 20), (width / werte.length / 1.5), (0 - d / max * (height -40) ));
					cr.fill();
				}else if (type == 1){
					cr.move_to(lastx - (width / werte.length), lasty);
					lasty = (height - d / max * (height -40) - 20);
					if (i >= 2){
						cr.line_to(lastx, (height - d / max * (height -40) - 20));
						cr.stroke ();
					}
				}
				lastx = lastx + (width / werte.length);
			}
			
	        return true;
	    }
	    
	    public override void size_allocate (Gtk.Allocation allocation) {
	        // The base method will save the allocation and move/resize the
	        // widget's GDK window if the widget is already realized.
	        base.size_allocate (allocation);
	    }
	}
}
